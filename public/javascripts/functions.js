function twoDates(date_now, date_past) {
  var seconds = (date_now.getTime() - date_past.getTime()) / 1000;
  return seconds;
}

function lastMessageCalculator(seconds) {
  if(seconds / 86400 > 1) {
    return Math.round(seconds/86400) + "days ago";
  } else if(seconds / 3600 > 1) {
    return Math.round(seconds/3600) + " hours ago...";
  } else if(seconds / 60 > 1) {
    return Math.round(seconds/60) + " minutes ago...";
  } else {
    return Math.round(seconds) + " seconds ago...";
  }
}

function getUrl() {
  if (typeof location.origin === 'undefined') {
    location.origin = location.protocol + '//' + location.host;
  }

  return location.origin;
}

function xhrGet(url) {
  var xhr = new XMLHttpRequest();
  xhr.open('GET', url, false);
  xhr.setRequestHeader("Content-type", "application/json;charset=UTF-8");
  xhr.send();

  xhr.addEventListener("load", processRequest, false);

  function processRequest(e) {
    if (xhr.readyState == 4 && xhr.status == 200) {
        var response = JSON.parse(xhr.responseText);
        return response;
    } else {
      return response;
    }
  }
}

function arrayToObject(arr) {
  var rv = {};
  for (var i = 0; i < arr.length; ++i)
    rv[i] = arr[i];
  return rv;
}

function convertTimesToJson(d) {
  var obj = {};
  for(var i=0; i < d.length; i++) {
     obj[d[i]] = d[i];
  }
  return obj;
}

function convertTypesToJson(d) {
  var obj = {};
  for(var i=0; i < d.length; i++) {
     obj[d[i].id] = d[i].name;
  }
  return obj;
}

function convertJsonToSweetAlert(d) {
  var str = "{";
  for(var i=0; i < d.length; i++) {
     var append = '"' + d[i]._id + '"' + ":" + '"' + d[i].name + '"';
     if(i == 0) {
       str = str + append;
     } else {
       str = str + "," + append;
     }
  }
  str = str + "}";
  obj = JSON.parse(str);
  return obj;
}

function cookieExists(name) {
  if (document.cookie.indexOf(name+"=") >= 0) {
    return true;
  } else {
    return false;
  }
}

function createCookie(name, value, days) {
    var expires;
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    }
    else expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name, "", -1);
}

function areCookiesEnabled() {
    var r = false;
    createCookie("testing", "Hello", 1);
    if (readCookie("testing") != null) {
        r = true;
        eraseCookie("testing");
    }
    return r;
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function xhrPost(url, token, data, fn) {
  var xhr = new XMLHttpRequest();
  xhr.open('POST', url, true);
  xhr.setRequestHeader("Content-type", "application/json;charset=UTF-8");
  xhr.setRequestHeader("x-access-token", token);
  xhr.send(JSON.stringify(data));

  xhr.addEventListener("load", processRequest, false);
  function processRequest(e) {
    if (xhr.readyState == 4) {
      var res = {
        response: JSON.parse(xhr.responseText),
        status: xhr.status
      }
      return fn(null, res);
    } else {
      return fn(404, null);
    }
  }
}

function isCheckedById(id) {
  return $('#' + id).is(":checked")
}

function inArray(array, a) {
  if (array.indexOf($.trim(a)) > -1) {
    return true;
  } else {
    return false;
  }
}

function validateArrayEmpty(arr, discardIndexes) {
  for(var i = 0; i < arr.length; i++) {
    if(!inArray(discardIndexes, i)) {
      if(arr[i] == "") {
        return true;
      }
    }
  }
  return false;
}
