var express = require('express');
var Lfg = require('../models/lfg');
var router = express.Router();
var jwt = require('jwt-simple')

router.post('/', function(req, res, next) {
  var utils = require('../utils/user_utils')(req);
  var validate = require('../utils/validation_utils')(req);
  var token = (req.body && req.body.access_token) || (req.query && req.query.access_token) || req.headers['x-access-token'];
  var decoded = jwt.decode(token, require('../../database').secret);
  var oauthID = decoded.user;

  utils.getUser(oauthID, function(err, user_profile){
    if(err && !user_profile) {
      res.status(400).send({"message" : "Could not find author."});
    } else if(!err && user_profile) {
      validate.validate_post(req.body, function(err) {
        if(err) {
          res.status(400).send({"message" : "Hmm, it does seem that one has inputted something wrong."});
        } else {
          newLfg = new Lfg({
            author: oauthID,
            platform: req.body.client,
            experience: req.body.experience,
            activity: req.body.gamemode,
            language: req.body.language,
            notes: req.body.notes
          }).save(function(err, item) {
            if(err) {
              res.status(400).send({"message" : "Something went wrong, try again?"});
            } else {
              res.status(200).send({"message" : "It has been done! (Post created)"})
            }
          });
          utils.emitLfg({
              author: oauthID,
              platform: req.body.client,
              experience: req.body.experience,
              activity: req.body.gamemode,
              language: req.body.language,
              notes: req.body.notes
            }, req.app.locals.global_emitter)
        }
      });
    } else if(!err && !user_profile) {
      res.status(400).send({"message" : "Could not find author."});
    }
  });

});

module.exports = router;
